import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';

interface TableColumn<T> {
    label: string;
    accessor: keyof T;
    render?: (rowData: T) => React.ReactNode;
    width?: string;
}

interface TableComponentProps<T> {
    data: T[];
    columns: TableColumn<T>[];
}

const TableComponent = <T extends Record<string, any>>({ data, columns }: TableComponentProps<T>) => {
    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        {columns.map((column) => (
                            <TableCell key={column.label} style={{ width: column.width }}>
                                {column.label}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, rowIndex) => (
                        <TableRow key={rowIndex}>
                            {columns.map((column) => (
                                <TableCell key={`${rowIndex}_${column.label}`} style={{ width: column.width }}>
                                    {column.render ? column.render(row) : row[column.accessor]}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};


export default TableComponent;