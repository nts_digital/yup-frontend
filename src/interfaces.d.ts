import React from "react";

export interface People {
    id: string,
    name: string
    height: number
    mass: number,
    gender: string,
    action?: React.ReactNode
}
