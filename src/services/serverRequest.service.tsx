import axios from "axios";

interface axiosInterface {
    type: 'get' | 'post' | 'patch' | 'delete',
    path: string,
    payload?: object
}

export const serverRequest: ({type, path, payload}: axiosInterface) => Promise<any> = async ({type, path, payload}) => {
    try {
        return  await axios({
            method: type,
            url: path,
            data: payload
        });
    } catch (e) {
        return e
    }
}