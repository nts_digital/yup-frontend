import React, {useEffect, useState} from 'react';
import './App.css';
import {
    Box,
    Button,
    Container,
    Dialog, DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, FormControl, Grid, InputLabel, MenuItem, Select, SelectChangeEvent,
    Tab,
    Tabs, TextField,
    Typography, useMediaQuery, useTheme
} from "@mui/material";
import TableComponent from "./components/table.component";
import {People} from "./interfaces";
import {serverRequest} from "./services/serverRequest.service";
import {AxiosResponse} from "axios";
import Loader from "./components/loader.component";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { v4 as uuidv4 } from 'uuid';

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function CustomTabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ pt: 3 }}>
                    <Typography component={'div'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

function App() {
    const [value, setValue] = useState(0);
    const [open, setOpen] = React.useState(false);
    const [isLoaded, setIsLoaded] = useState(false)
    const [peoples, setPeoples] = useState<People[]>([])
    const [savedPeoples, setSavedPeoples] = useState<People[]>([])
    const theme = useTheme();
    const isMdScreen = useMediaQuery(theme.breakpoints.up('md'));
    const [gender, setGender] = React.useState('');

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };


    useEffect(() => {
        serverRequest({type: "get", path: '/api/people'})
            .then((response: AxiosResponse) => {
                if (response.status === 200) {
                    setPeoples(response.data)
                    setIsLoaded(true)
                } else {
                    console.error('Failed to fetch data:', response.statusText);
                }
            })
            .catch((error) => {
                console.error('Error fetching data:', error.message);
            });

        serverRequest({type: "get", path: '/api/saved_people'})
            .then((response: AxiosResponse) => {
                if (response.status === 200) {
                    setSavedPeoples(response.data)
                } else {
                    console.error('Failed to fetch data:', response.statusText);
                }
            })
            .catch((error) => {
                console.error('Error fetching data:', error.message);
            });

    }, [])

    const columns = [
        { label: 'Имя', accessor: 'name' as const },
        { label: 'Рост', accessor: 'height' as const },
        { label: 'Вес', accessor: 'mass' as const },
        { label: 'Пол', accessor: 'gender' as const },
        {
            label: '',
            accessor: 'action' as const,
            width: '100px',
            render: (rowData: People) => {
                return (
                    <Button variant="outlined" size="small" onClick={() => handleSavePeople(rowData)}>Сохранить</Button>
                );
            }
        }
    ]

    const savedColumns = [
        { label: 'Имя', accessor: 'name' as const },
        { label: 'Рост', accessor: 'height' as const },
        { label: 'Вес', accessor: 'mass' as const },
        { label: 'Пол', accessor: 'gender' as const },
        {
            label: '',
            accessor: 'action' as const,
            width: '100px',
            render: (rowData: People) => {
                return (
                    <Button variant="outlined" size="small" color="error" onClick={() => handleDeletePeople(rowData)}>Удалить</Button>
                );
            }
        }
    ]

    const handleSavePeople = (rowData: People) => {
        serverRequest({type: "post", path: '/api/people', payload: rowData})
            .then((response: AxiosResponse) => {
                if (response.status === 200) {
                    setSavedPeoples((prevSavedPeoples) => [...prevSavedPeoples, rowData]);
                    if (open) {
                        setOpen(false)
                    }
                    toast.success(`Герой ${rowData.name} успешно сохранен`)
                } else {
                    console.error('Failed to save data:', response.statusText)
                    toast.error('Произошла ошибка при сохранении героя')
                }
            })
            .catch((error) => {
                console.error('Error saving data:', error.message)
                toast.error('Произошла ошибка при сохранении героя')
            });
    }

    const handleDeletePeople = (rowData: People) => {
        serverRequest({type: "delete", path: '/api/people/' + rowData.id})
            .then((response: AxiosResponse) => {
                if (response.status === 202) {
                    setSavedPeoples((prevSavedPeoples) => {
                        return prevSavedPeoples.filter(person => person.id !== rowData.id);
                    });
                    toast.success(`Герой ${rowData.name} успешно удален`)
                } else {
                    console.error('Failed to delete data:', response);
                    toast.error('Произошла ошибка при удалении героя')
                }
            })
            .catch((error) => {
                console.error('Error deleting data:', error.message)
                toast.error('Произошла ошибка при удалении героя')
            });
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleGenderChange = (event: SelectChangeEvent) => {
        setGender(event.target.value);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const data = new FormData(event.currentTarget)

        const payload = {
            id: uuidv4(),
            name: data.get('name') as string,
            height: Number(data.get('height') as string),
            mass: Number(data.get('mass') as string),
            gender: data.get('gender') as string
        }

        handleSavePeople(payload)
    }

    return (
        <Container maxWidth='xl'>
            <ToastContainer position="bottom-center"/>
            <Box sx={{my: 6}}>
                <Typography variant='h4'>
                    The Star Wars
                </Typography>
            </Box>

            <Button variant="outlined" onClick={handleClickOpen}>
                Добавить своего героя
            </Button>

            <Dialog open={open} onClose={handleClose}>
                <form onSubmit={handleSubmit}>
                    <DialogTitle>Свой герой</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Введите информацию о герое и нажммите кнопку "Добавить"
                        </DialogContentText>
                            <Grid container spacing={2} padding={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        name="name"
                                        label="Имя героя"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} md={4} sx={{ paddingRight: isMdScreen ? 2 : 0 }}>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        name="height"
                                        label="Рост героя"
                                        type="number"
                                        fullWidth
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} md={4} sx={{ paddingRight: isMdScreen ? 2 : 0 }}>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        name="mass"
                                        label="Вес героя"
                                        type="number"
                                        fullWidth
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} md={4}>
                                    <FormControl fullWidth variant="standard" margin="dense">
                                        <InputLabel>Пол героя</InputLabel>
                                        <Select
                                            name="gender"
                                            label="Пол героя"
                                            value={gender}
                                            onChange={handleGenderChange}
                                        >
                                            <MenuItem value={'male'}>male</MenuItem>
                                            <MenuItem value={'female'}>female</MenuItem>
                                            <MenuItem value={'n/a'}>n/a</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Отменить</Button>
                        <Button type="submit">Добавить</Button>
                    </DialogActions>
                </form>
            </Dialog>

            <Box sx={{width: '100%'}}>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs value={value} onChange={handleTabChange} aria-label="basic tabs example">
                        <Tab label="Герои" {...a11yProps(0)} />
                        <Tab label="Сохранённые" {...a11yProps(1)} />
                    </Tabs>
                </Box>
                <CustomTabPanel value={value} index={0}>
                    {isLoaded ? <TableComponent data={peoples} columns={columns}/> : <Loader />}
                </CustomTabPanel>
                <CustomTabPanel value={value} index={1}>
                    {savedPeoples.length === 0
                        ? <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '50vh' }} component={'span'}>Список сохраненных героев пуст</Typography>
                        : <TableComponent data={savedPeoples} columns={savedColumns}/> }
                </CustomTabPanel>
            </Box>
        </Container>
  );
}

export default App;
