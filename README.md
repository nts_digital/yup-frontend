# The Star Wars
[![Generic badge](https://img.shields.io/badge/Версия-1.0.0-<COLOR>.svg)](https://gitlab.com/nts_digital/yup-frontend.git)
[![Npm package version](https://badgen.net/npm/v/react)](https://npmjs.com/package/react)
[![TypeScript](https://img.shields.io/badge/--3178C6?logo=typescript&logoColor=ffffff)](https://www.typescriptlang.org/)

### Запуск проекта в DEV среде

1. В файле package.json указать адрес backend сервера в ключе "proxy"
2. Выполнить установку зависимостей командой `npm install`
3. Выполнить в терминале команду `npm start` для запуска проекта
4. Открыть [http://localhost:3000](http://localhost:3000) в браузере


### Запуск проекта в PROD среде на примере NGINX

1. Выполнить установку зависимостей командой `npm install`
2. Выполнить в терминале команду `npm run build`
3. В виртуальном хосте nginx указать корневую директорию <project-root>/build
4. Прописать proxy_pass в виртуальном хосте nginx

```
location /api {
    proxy_pass http://<server-url>/api;
}
```
